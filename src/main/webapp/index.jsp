<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!--meta http-equiv="refresh" content="1">-->
    </head>
    <%
        String color=request.getParameter("color");
        String[] colors={"blue","red","yellow","orange","brown","green","gray","pink","white","black"};
        if (color==null) color=colors[(int)(Math.random()*5)];
        
        String size=request.getParameter("size");
        int x=(int)(Math.random()*100);
        if (size==null) size=(x+50)+"px";
        
        String divcolor=request.getParameter("divcolor");
        if (divcolor==null) {
            while (true){
            divcolor=colors[(int)(Math.random()*5)];
            if (divcolor!=color) break;
            }
        }
    %>
    <body bgcolor="<%=color%>">
        <div style="font-size:<%=size%>;color:<%=divcolor%>">Hello World!</div>
    </body>
</html>

